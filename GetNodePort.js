// Parse the output of this kubetrl command:
//	kubectl get service hello -o json
// to retrieve the nodePort of the service.

var kubectloutput=process.argv[2]
var nodePort = JSON.parse(kubectloutput).spec.ports[0].nodePort;
if (nodePort == null) {
	throw "node port not found";
} else {
	console.log(nodePort);
}
