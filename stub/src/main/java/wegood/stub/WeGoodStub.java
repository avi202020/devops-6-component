package wegood.stub;

import java.time.LocalTime;

public interface WeGoodStub {
    public boolean areWeGood() throws Exception;
	public void setTime(LocalTime time) throws Exception;
}
